<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'debug.argument_resolver.service' shared service.

return $this->privates['debug.argument_resolver.service'] = new \Symfony\Component\HttpKernel\Controller\ArgumentResolver\TraceableValueResolver(new \Symfony\Component\HttpKernel\Controller\ArgumentResolver\ServiceValueResolver(new \Symfony\Component\DependencyInjection\ServiceLocator(array('App\\Controller\\CategoryController::removeCategory' => function () {
    return ($this->privates['.service_locator.koD3dBr'] ?? $this->load('get_ServiceLocator_KoD3dBrService.php'));
}, 'App\\Controller\\CategoryController::update' => function () {
    return ($this->privates['.service_locator.koD3dBr'] ?? $this->load('get_ServiceLocator_KoD3dBrService.php'));
}, 'App\\Controller\\HomeController::index' => function () {
    return ($this->privates['.service_locator.d1PLOFT'] ?? $this->load('get_ServiceLocator_D1PLOFTService.php'));
}, 'App\\Controller\\LoginController::index' => function () {
    return ($this->privates['.service_locator.xJ_gBvk'] ?? $this->load('get_ServiceLocator_XJGBvkService.php'));
}, 'App\\Controller\\LoginController::update' => function () {
    return ($this->privates['.service_locator.bgl2V9x'] ?? $this->load('get_ServiceLocator_Bgl2V9xService.php'));
}, 'App\\Controller\\ProductCategoryController::show' => function () {
    return ($this->privates['.service_locator.QRcwMV3'] ?? $this->load('get_ServiceLocator_QRcwMV3Service.php'));
}, 'App\\Controller\\ProductController::index' => function () {
    return ($this->privates['.service_locator.ESSj2sg'] ?? $this->load('get_ServiceLocator_ESSj2sgService.php'));
}, 'App\\Controller\\ProductController::remove' => function () {
    return ($this->privates['.service_locator.4nPGKhY'] ?? $this->load('get_ServiceLocator_4nPGKhYService.php'));
}, 'App\\Controller\\ProductController::show' => function () {
    return ($this->privates['.service_locator.d1PLOFT'] ?? $this->load('get_ServiceLocator_D1PLOFTService.php'));
}, 'App\\Controller\\ProductController::update' => function () {
    return ($this->privates['.service_locator.Q_Eg7Vo'] ?? $this->load('get_ServiceLocator_QEg7VoService.php'));
}, 'App\\Controller\\ProductLineController::index' => function () {
    return ($this->privates['.service_locator.4ZNe2Z_'] ?? $this->load('get_ServiceLocator_4ZNe2ZService.php'));
}, 'App\\Controller\\ProductLineController::remove' => function () {
    return ($this->privates['.service_locator.X3GoigQ'] ?? $this->load('get_ServiceLocator_X3GoigQService.php'));
}, 'App\\Controller\\ShoppingCartController::index' => function () {
    return ($this->privates['.service_locator.1ldS71l'] ?? $this->load('get_ServiceLocator_1ldS71lService.php'));
}, 'App\\Controller\\SignUpController::index' => function () {
    return ($this->privates['.service_locator.DcgCYKP'] ?? $this->load('get_ServiceLocator_DcgCYKPService.php'));
}, 'App\\Controller\\CategoryController:removeCategory' => function () {
    return ($this->privates['.service_locator.koD3dBr'] ?? $this->load('get_ServiceLocator_KoD3dBrService.php'));
}, 'App\\Controller\\CategoryController:update' => function () {
    return ($this->privates['.service_locator.koD3dBr'] ?? $this->load('get_ServiceLocator_KoD3dBrService.php'));
}, 'App\\Controller\\HomeController:index' => function () {
    return ($this->privates['.service_locator.d1PLOFT'] ?? $this->load('get_ServiceLocator_D1PLOFTService.php'));
}, 'App\\Controller\\LoginController:index' => function () {
    return ($this->privates['.service_locator.xJ_gBvk'] ?? $this->load('get_ServiceLocator_XJGBvkService.php'));
}, 'App\\Controller\\LoginController:update' => function () {
    return ($this->privates['.service_locator.bgl2V9x'] ?? $this->load('get_ServiceLocator_Bgl2V9xService.php'));
}, 'App\\Controller\\ProductCategoryController:show' => function () {
    return ($this->privates['.service_locator.QRcwMV3'] ?? $this->load('get_ServiceLocator_QRcwMV3Service.php'));
}, 'App\\Controller\\ProductController:index' => function () {
    return ($this->privates['.service_locator.ESSj2sg'] ?? $this->load('get_ServiceLocator_ESSj2sgService.php'));
}, 'App\\Controller\\ProductController:remove' => function () {
    return ($this->privates['.service_locator.4nPGKhY'] ?? $this->load('get_ServiceLocator_4nPGKhYService.php'));
}, 'App\\Controller\\ProductController:show' => function () {
    return ($this->privates['.service_locator.d1PLOFT'] ?? $this->load('get_ServiceLocator_D1PLOFTService.php'));
}, 'App\\Controller\\ProductController:update' => function () {
    return ($this->privates['.service_locator.Q_Eg7Vo'] ?? $this->load('get_ServiceLocator_QEg7VoService.php'));
}, 'App\\Controller\\ProductLineController:index' => function () {
    return ($this->privates['.service_locator.4ZNe2Z_'] ?? $this->load('get_ServiceLocator_4ZNe2ZService.php'));
}, 'App\\Controller\\ProductLineController:remove' => function () {
    return ($this->privates['.service_locator.X3GoigQ'] ?? $this->load('get_ServiceLocator_X3GoigQService.php'));
}, 'App\\Controller\\ShoppingCartController:index' => function () {
    return ($this->privates['.service_locator.1ldS71l'] ?? $this->load('get_ServiceLocator_1ldS71lService.php'));
}, 'App\\Controller\\SignUpController:index' => function () {
    return ($this->privates['.service_locator.DcgCYKP'] ?? $this->load('get_ServiceLocator_DcgCYKPService.php'));
}))), ($this->privates['debug.stopwatch'] ?? $this->privates['debug.stopwatch'] = new \Symfony\Component\Stopwatch\Stopwatch(true)));
