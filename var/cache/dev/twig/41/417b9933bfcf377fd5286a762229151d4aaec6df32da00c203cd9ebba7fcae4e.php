<?php

/* product/show.html.twig */
class __TwigTemplate_517f7e3c8a918f5f7775c6602cadfbbaa21c0ca367c367c56d28b281925db750 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "product/show.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "product/show.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "product/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo "    <title>
        Product
    </title>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "
<div class=\"container-fluid\">
    <div class=\"col-md-5 mx-auto\">
        <div class=\"card mb-3\" id=\"product-page\">
            <h3 class=\"card-header\">";
        // line 14
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new Twig_Error_Runtime('Variable "product" does not exist.', 14, $this->source); })()), "name", array()), "html", null, true);
        echo "</h3>
            <div class=\"card-body\">
            </div>
            <img style=\"height: 710 px; width: 100%; display: block;\" src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("uploads/images/" . twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new Twig_Error_Runtime('Variable "product" does not exist.', 17, $this->source); })()), "image", array()))), "html", null, true);
        echo "\" alt=\"Card image\">
            <div class=\"list-group\">
                    <a href=\"#\" class=\"list-group-item list-group-item-action flex-column align-items-start active\">
                      <div class=\"d-flex w-100 justify-content-between\">
                        <h5 class=\"mb-1\">List group item heading</h5>
                      </div>
                      <p class=\"mb-1\"> ";
        // line 23
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new Twig_Error_Runtime('Variable "product" does not exist.', 23, $this->source); })()), "description", array()), "html", null, true);
        echo " </p>
                    </a>
                  </div>
            <ul class=\"list-group list-group-flush\">
            <li class=\"list-group-item\">Catégorie: ";
        // line 27
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new Twig_Error_Runtime('Variable "product" does not exist.', 27, $this->source); })()), "category", array()), "name", array()), "html", null, true);
        echo "</li>
            <li class=\"list-group-item\">Price: ";
        // line 28
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new Twig_Error_Runtime('Variable "product" does not exist.', 28, $this->source); })()), "price", array()), "html", null, true);
        echo " €</li>

            </ul>

            <div class=\"card-body\">
                <form method=\"post\" action=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("product_line", array("id" => twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new Twig_Error_Runtime('Variable "product" does not exist.', 33, $this->source); })()), "id", array()))), "html", null, true);
        echo "\">
                  <input type=\"number\" name=\"number\">
                  <button class=\"btn btn-secondary\">Add</button>
                </form>
              </div>

            <div class=\"card-body\">
            <a class=\"btn btn-info\" href=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("product_line", array("id" => twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new Twig_Error_Runtime('Variable "product" does not exist.', 40, $this->source); })()), "id", array()))), "html", null, true);
        echo "\">Add to cart</a>
            </div>
            ";
        // line 43
        echo "            <div class=\"card-footer text-muted\">
                <a class=\"btn btn-info\" href=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("remove_product", array("id" => twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new Twig_Error_Runtime('Variable "product" does not exist.', 44, $this->source); })()), "id", array()))), "html", null, true);
        echo "\">Remove Product</a>
            </div>
            <div class=\"card-footer text-muted\">
                <a class=\"btn btn-info\" href=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("update_product", array("id" => twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new Twig_Error_Runtime('Variable "product" does not exist.', 47, $this->source); })()), "id", array()))), "html", null, true);
        echo "\">Update Product</a>
            </div>
            ";
        // line 50
        echo "        </div>
    </div>
</div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "product/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  145 => 50,  140 => 47,  134 => 44,  131 => 43,  126 => 40,  116 => 33,  108 => 28,  104 => 27,  97 => 23,  88 => 17,  82 => 14,  76 => 10,  67 => 9,  54 => 4,  45 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block title %}
    <title>
        Product
    </title>
{% endblock %}

{% block body %}

<div class=\"container-fluid\">
    <div class=\"col-md-5 mx-auto\">
        <div class=\"card mb-3\" id=\"product-page\">
            <h3 class=\"card-header\">{{product.name}}</h3>
            <div class=\"card-body\">
            </div>
            <img style=\"height: 710 px; width: 100%; display: block;\" src=\"{{asset('uploads/images/'~product.image)}}\" alt=\"Card image\">
            <div class=\"list-group\">
                    <a href=\"#\" class=\"list-group-item list-group-item-action flex-column align-items-start active\">
                      <div class=\"d-flex w-100 justify-content-between\">
                        <h5 class=\"mb-1\">List group item heading</h5>
                      </div>
                      <p class=\"mb-1\"> {{product.description}} </p>
                    </a>
                  </div>
            <ul class=\"list-group list-group-flush\">
            <li class=\"list-group-item\">Catégorie: {{product.category.name}}</li>
            <li class=\"list-group-item\">Price: {{product.price}} €</li>

            </ul>

            <div class=\"card-body\">
                <form method=\"post\" action=\"{{path('product_line', {id:product.id})}}\">
                  <input type=\"number\" name=\"number\">
                  <button class=\"btn btn-secondary\">Add</button>
                </form>
              </div>

            <div class=\"card-body\">
            <a class=\"btn btn-info\" href=\"{{path('product_line', {id:product.id})}}\">Add to cart</a>
            </div>
            {# {% if is_granted('IS_AUTHENTICATED_REMEMBERED') %} #}
            <div class=\"card-footer text-muted\">
                <a class=\"btn btn-info\" href=\"{{path('remove_product', {id:product.id})}}\">Remove Product</a>
            </div>
            <div class=\"card-footer text-muted\">
                <a class=\"btn btn-info\" href=\"{{path('update_product', {id:product.id})}}\">Update Product</a>
            </div>
            {# {% endif %} #}
        </div>
    </div>
</div>

{% endblock %}", "product/show.html.twig", "/application/templates/product/show.html.twig");
    }
}
