<?php

/* home/index.html.twig */
class __TwigTemplate_e68e21eab1e40caf6e6a2a229f4936cce64fe1b8405e0fae6ba62c2a94b0a637 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "home/index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "home/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "home/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function block_title($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 2
        echo "<title>
  Home
</title>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "


  <!-- Page Content -->
  <div class=\"container\">

    <!-- Jumbotron Header -->
    <div class=\"jumbotron\">
      <h1 class=\"display-3\">The Future Sound</h1>

      <hr class=\"my-4\">
      <p>Sure, it’s not perfect (*cough* “No more dead cops!”), but it remains a gripping, beautifully acted film even after
        dozens of viewings. And it is still the world’s finest superhero movie, despite a deluge of costume-clad contenders
        that have taken to the big screen in the decade since its release.</p>
      <p class=\"lead\">
        <a class=\"btn btn-info btn-lg\" href=\"#\" role=\"button\">Shop</a>
      </p>
    </div>


    <!-- Page Features -->
    <div class=\"row text-center\">
      ";
        // line 28
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new Twig_Error_Runtime('Variable "result" does not exist.', 28, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
            // line 29
            echo "
      <div class=\"col-lg-4 col-md-6 mb-4\">
        <div class=\"card\">
          <img class=\"card-img-top mx-auto\" src=\"";
            // line 32
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("uploads/images/" . twig_get_attribute($this->env, $this->source, $context["row"], "image", array()))), "html", null, true);
            echo "\" alt=\"\">
          <div class=\"card-body\">
            <h4 class=\"card-title\">";
            // line 34
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["row"], "name", array()), "html", null, true);
            echo "</h4>
            <p class=\"card-text\">";
            // line 35
            echo twig_escape_filter($this->env, twig_slice($this->env, twig_get_attribute($this->env, $this->source, $context["row"], "description", array()), 0, 20), "html", null, true);
            echo " ...</p>
          </div>
          <div class=\"card-body\">
            <p class=\"card-text\">";
            // line 38
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["row"], "price", array()), "html", null, true);
            echo " €</p>
          </div>
          <div class=\"card-footer\">
            <a href=\"";
            // line 41
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("product", array("id" => twig_get_attribute($this->env, $this->source, $context["row"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-info\">See Product</a>
          </div>
        </div>
      </div>

      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 47
        echo "
    </div>
    <!-- /.row -->
  </div>

  

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "home/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  141 => 47,  129 => 41,  123 => 38,  117 => 35,  113 => 34,  108 => 32,  103 => 29,  99 => 28,  75 => 6,  66 => 5,  53 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %} {% block title %}
<title>
  Home
</title>
{% endblock %} {% block body %}



  <!-- Page Content -->
  <div class=\"container\">

    <!-- Jumbotron Header -->
    <div class=\"jumbotron\">
      <h1 class=\"display-3\">The Future Sound</h1>

      <hr class=\"my-4\">
      <p>Sure, it’s not perfect (*cough* “No more dead cops!”), but it remains a gripping, beautifully acted film even after
        dozens of viewings. And it is still the world’s finest superhero movie, despite a deluge of costume-clad contenders
        that have taken to the big screen in the decade since its release.</p>
      <p class=\"lead\">
        <a class=\"btn btn-info btn-lg\" href=\"#\" role=\"button\">Shop</a>
      </p>
    </div>


    <!-- Page Features -->
    <div class=\"row text-center\">
      {% for row in result %}

      <div class=\"col-lg-4 col-md-6 mb-4\">
        <div class=\"card\">
          <img class=\"card-img-top mx-auto\" src=\"{{asset('uploads/images/'~row.image)}}\" alt=\"\">
          <div class=\"card-body\">
            <h4 class=\"card-title\">{{row.name}}</h4>
            <p class=\"card-text\">{{row.description[:20]}} ...</p>
          </div>
          <div class=\"card-body\">
            <p class=\"card-text\">{{row.price}} €</p>
          </div>
          <div class=\"card-footer\">
            <a href=\"{{path('product',{id:row.id})}}\" class=\"btn btn-info\">See Product</a>
          </div>
        </div>
      </div>

      {% endfor %}

    </div>
    <!-- /.row -->
  </div>

  

{% endblock %}", "home/index.html.twig", "/application/templates/home/index.html.twig");
    }
}
