<?php

/* admin/index.html.twig */
class __TwigTemplate_d57b28a369ad51acc1df6fbd25e13e2f63d0ccb29f47f3c9444a6cdd36a859bd extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "admin/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "admin/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "admin/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "<div class=\"container-fluid\">
    <div class=\"row d-flex m-3\">

        <div class=\"col-4\">
            <div class=\"card\">
                <div class=\"card-body\">
                    <h5 class=\"card-title\">PRODUCT</h5>

                    ";
        // line 13
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 14
            echo "                    <a href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("add_product");
            echo "\" class=\"main\">
                        <button class=\"btn btn-secondary\">Create products</button>
                    </a>
                    ";
        }
        // line 18
        echo "
                </div>
            </div>
        </div>

        <div class=\"col-4\">
            <div class=\"card\">
                <div class=\"card-body\">
                    <h5 class=\"card-title\">USER</h5>

                    ";
        // line 28
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 29
            echo "                    <a href=\"#\" class=\"adminButton\">
                        <button class=\"btn btn-primary\">History</button>
                    </a>
                    <a href=\"#\" class=\"adminButton\">
                            <button class=\"btn btn-primary\">Edit compte</button>
                        </a>
                    ";
        }
        // line 36
        echo "
                </div>
            </div>
        </div>

        <div class=\"col-4\">
            <div class=\"card\">
                <div class=\"card-body\">
                    <h5 class=\"card-title\">CATEGORIES</h5>

                    ";
        // line 46
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 47
            echo "                    <a href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("add_category");
            echo "\" class=\"adminButton\">
                        <button class=\"btn btn-success btn-lg\">Create Category</button>
                    </a>
                    <a href=\"";
            // line 50
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("edit_category");
            echo "\">
                        <button class=\"btn btn-warning btn-lg\">Edit Categories</button>
                    </a>
                    <a href=\"";
            // line 53
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("select_remove_category");
            echo "\">
                        <button class=\"btn btn-danger btn-lg\">Supp Categories</button>
                    </a>
                    ";
        }
        // line 57
        echo "
                </div>
            </div>
        </div>

    </div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "admin/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  130 => 57,  123 => 53,  117 => 50,  110 => 47,  108 => 46,  96 => 36,  87 => 29,  85 => 28,  73 => 18,  65 => 14,  63 => 13,  53 => 5,  44 => 4,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}


{% block body %}
<div class=\"container-fluid\">
    <div class=\"row d-flex m-3\">

        <div class=\"col-4\">
            <div class=\"card\">
                <div class=\"card-body\">
                    <h5 class=\"card-title\">PRODUCT</h5>

                    {% if is_granted('IS_AUTHENTICATED_REMEMBERED') %}
                    <a href=\"{{path('add_product')}}\" class=\"main\">
                        <button class=\"btn btn-secondary\">Create products</button>
                    </a>
                    {% endif %}

                </div>
            </div>
        </div>

        <div class=\"col-4\">
            <div class=\"card\">
                <div class=\"card-body\">
                    <h5 class=\"card-title\">USER</h5>

                    {% if is_granted('IS_AUTHENTICATED_REMEMBERED') %}
                    <a href=\"#\" class=\"adminButton\">
                        <button class=\"btn btn-primary\">History</button>
                    </a>
                    <a href=\"#\" class=\"adminButton\">
                            <button class=\"btn btn-primary\">Edit compte</button>
                        </a>
                    {% endif %}

                </div>
            </div>
        </div>

        <div class=\"col-4\">
            <div class=\"card\">
                <div class=\"card-body\">
                    <h5 class=\"card-title\">CATEGORIES</h5>

                    {% if is_granted('IS_AUTHENTICATED_REMEMBERED') %}
                    <a href=\"{{path('add_category')}}\" class=\"adminButton\">
                        <button class=\"btn btn-success btn-lg\">Create Category</button>
                    </a>
                    <a href=\"{{path('edit_category')}}\">
                        <button class=\"btn btn-warning btn-lg\">Edit Categories</button>
                    </a>
                    <a href=\"{{path('select_remove_category')}}\">
                        <button class=\"btn btn-danger btn-lg\">Supp Categories</button>
                    </a>
                    {% endif %}

                </div>
            </div>
        </div>

    </div>
</div>
{% endblock %}
", "admin/index.html.twig", "/application/templates/admin/index.html.twig");
    }
}
